/**
 * Created by serg on 15.10.14.
 */

app.controller('AddMix',function($scope,MixesService,$location){
    $scope.name = 'name';
    $scope.author = 'author';
    $scope.lifeTime = 'lifetime';
    $scope.addMix = MixesService.addMix;
    $scope.go = function ( path ) {
	  $location.path( path );
	};
});