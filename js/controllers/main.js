/**
 * Created by serg on 15.10.14.
 */
app.controller('Main',function($scope,MixesService){
    $scope.mixes = MixesService.getMixes();
    $scope.id = 0;
    $scope.removeMix = MixesService.removeMix;
    $scope.dialog = function(id){
    	$scope.id = parseInt(id);
    	$('#dialog').show();
  		$("#dialog").dialog();
    };
    $scope.modifyMix = MixesService.modifyMix;
});