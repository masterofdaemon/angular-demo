/**
 * Created by serg on 15.10.14.
 */

app.controller('Mix',function($scope,$routeParams,MixesService,$location){
    $scope.mix = MixesService.getMixById(parseInt($routeParams.mixId));
    $scope.removeMix = MixesService.removeMix;
    $scope.go = function ( path ) {
	  $location.path( path );
	};
	$scope.dialog = function(id){
    	$scope.id = parseInt(id);
    	$('#dialog').show();
  		$("#dialog").dialog();
    };
    $scope.modifyMix = MixesService.modifyMix;
});
