/**
 * Created by serg on 13.10.14.
 */

var app = angular.module('demo-app', ['ngRoute']);

//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider,$locationProvider) {
    $routeProvider.when('/main',
        {
            controller: 'Main',
            templateUrl: '/../templates/main.html'
        }).when('/mix/:mixId',
        {
            controller: 'Mix',
            templateUrl: '/../templates/mix.html'
        }).when('/addMix',
        {
            controller: 'AddMix',
            templateUrl: '/../templates/addMix.html'

        }).otherwise({ redirectTo: '/main' });
    //$locationProvider.html5Mode(true);
});