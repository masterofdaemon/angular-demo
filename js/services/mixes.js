/**
 * Created by serg on 15.10.14.
 */

app.service('MixesService', function () {
    this.getMixes = function () {
        return mixes;
    };

    this.removeMix = function(id){
        mixes.forEach(function(curr,i){if(curr.id === id) mixes.splice(i,1); });
    };

    this.addMix = function(author,name,lifetime){
        var res =_.max(mixes, function(mix){ return mix.id; });
        var id = res.id+1;
        mixes.push({id:id,author:author,name:name,lifetime:lifetime});
    };

    this.getMixById = function(id){
        var mix={};
        mixes.forEach(function(curr,i){if(curr.id==id) mix=curr; });
        return mix;
    };

    this.modifyMix = function(author,name,lifeTime,id){
        var vi = 0;
       mixes.forEach(function(curr,i){if(curr.id==id) vi=i; });
       mixes[vi].author = author;
       mixes[vi].name = name;
       mixes[vi].lifeTime = parseInt(lifeTime);
    }

    var mixes = [
        {id:1,author:'Marilyn Manson',name:"Vain",lifeTime:100},
        {id:2,author:'Marilyn Manson',name:"Nobodies",lifeTime:90},
        {id:3,author:'Marilyn Manson',name:"Beautiful people",lifeTime:100}
    ];

});